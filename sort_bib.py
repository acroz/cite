#!/usr/bin/env python

"""
sort_bib.py

Sorts a bibtex file alphabetically by key, so that diffs between
Mendeley exports will be more orderley. It also enforces some
formatting standards to the 'type' of @phdthesis entries.

Assumptions:
 - Entries begin with line as '@bibkey{PaperKey,'
 - Entries end with line containing only '}'
"""

import sys

bibkeys = ['article',
           'phdthesis',
           'techreport',
           'inproceedings',
           'misc']

def isstart(line):
    for bk in bibkeys:
        if line.startswith('@{0}{{'.format(bk)):
            return True
    return False

def parse_bibtex(fp):

    data = {}
    in_record = False
    
    for line in fp:
        if isstart(line):
            bibkey = line.split('{')[0].strip('@')
            key = line.split('{')[-1].strip(',\n')
            content = []
            in_record = True
            continue

        elif line.strip() == '}':
            if bibkey == 'phdthesis':
                # Add type if missing
                for line in content:
                    if line.startswith('type = {'):
                        break
                else:
                    content[-1] += ','
                    content.append('type = {{PhD} {T}hesis}')
            data[key] = (bibkey, content)
            in_record = False
            continue

        elif in_record:
            if bibkey == 'phdthesis' and line.startswith('type = {'):
                line = line.replace('MD', '{MD}')
                line = line.replace('Thesis', '{T}hesis')
                line = line.replace('thesis', '{T}hesis')

            # Fix Norwegian names
            line = line.replace(r'\O yvind', r'{\O}yvind')
            line = line.replace(r'T\o ndel', r'T{\o}ndel')

            content.append(line.strip('\n'))

    return data

def write_entry(bibkey, key, content, fp=sys.stdout):
    fp.write('@{0}{{{1},\n'.format(bibkey, key))
    for line in content:
        fp.write(line+'\n')
    fp.write('}\n')

if __name__ == '__main__':

    try:
        filename = sys.argv[1]
    except IndexError:
        sys.exit('Missing input filename')

    with open(sys.argv[1], 'r') as f:
        dat = parse_bibtex(f)

    for key in sorted(dat.keys()):
        bibkey, content = dat[key]
        write_entry(bibkey, key, content)
